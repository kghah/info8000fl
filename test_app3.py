from flask import Flask, escape, request, jsonify

app = Flask(__name__)

@app.route('/')
def fun():
    name = request.args.get("name","World")
    return jsonify({"name":name})